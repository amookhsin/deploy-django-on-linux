# استقرارش جنگو در سرور لینوکس اوبونتو

گام‌به‌گام:

1. به‌روزیدن بسته‌های سیستم‌عامل:

   <div dir="ltr" align="left" markdown="1">

   ```shell
   sudo apt update && sudo apt -y upgrade
   ```

   </div>

2. آمادن پایتون (یکی از امکان‌ها استفاده از محیط‌مجازی است):

   1. نصبیدن محیط‌مجازی پایتون:

      <div dir="ltr" align="left" markdown="1">

      ```shell
      sudo apt install python3-venv
      ```

      </div>

   2. ساختن محیط‌مجازی:

      <div dir="ltr" align="left" markdown="1">

      ```shell
      python -m venv /app/venv
      ```

      </div>

3. بارگذاردن برکا (app) روی سرور (یکی از امکان‌ها استفاده از یه مخزن است)

   1. نصبیدن گیت

      <div dir="ltr" align="left" markdown="1">

      ```shell
      sudo apt install git
      ```

      </div>

   2. پیکریدن گیت

      1. پایه‌ای:

         <div dir="ltr" align="left" markdown="1">

         ```shell
         git config --global user.name "your name"
         git config --global user.email "your_email@gmail.com"
         ```

         </div>

      2. وصلیدن به گیت‌هاب/گیت‌لب با SSH:

         1. ساختن یه کلید SSH:

            <div dir="ltr" align="left" markdown="1">

            ```shell
            ssh-keygen -t rsa -C "your_email@gmail.com" -b 4096 -f ~/.ssh/gitlab -N "YOUR_PASSPHRASE"
            ```

            </div>

         2. افزودن کلید عمومی به حساب [گیت‌لب](https://gitlab.com/-/profile/keys)/[گیت‌هاب](https://github.com/settings/keys):

            <div dir="ltr" align="left" markdown="1">

            ```shell
            cat ~/.ssh/gitlab.pub
            ```

            </div>

         - وصلیدن به چندین حساب گیت‌لب/گیت‌هاب با SSH:

           1. ایجادیدن فایل `~/.ssh/config`:

              <div dir="ltr" align="left" markdown="1">

              ```shell
              nano ~/.ssh/config
              ```

              </div>

           2. تعریفیدن میزبان‌ها:

              <div dir="ltr" align="left" markdown="1">

              ```text
              Host my_project
                  HostName gitlab.com
                  User git
                  PreferredAuthentications publickey
                  IdentityFile ~/.ssh/gitlab_id_rsa
              ```

              </div>

           3. استفیدن از میزبان‌اِ:

              <div dir="ltr" align="left" markdown="1">

              ```shell
              git remote add origin my_project:<project-address>
              ```

              </div>

4. آمادن پایگاه‌داده پستگرس‌کیوال:

   1. نصبیدن نیازمندی‌ها:

      <div dir="ltr" align="left" markdown="1">

      ```shell
      sudo apt install postgresql

      systemctl status postgresql
      ```

      </div>

   2. پیکریدن پایگاه‌داده پستگرس‌کیوال:

       <div dir="ltr" align="left" markdown="1">

      ```shell
      sudo -u postgres psql
      ```

       </div>

      1. ایجادیدن پایگاه‌داده:

         <div dir="ltr" align="left" markdown="1">

         ```shell
         psql (12.6 (Ubuntu 12.6-0ubuntu0.20.04.1))
         Type "help" for help.

         postgres=# CREATE DATABASE db_name;
         ```

         </div>

         - دیدن پایگاه‌داده‌ها:

           <div dir="ltr" align="left" markdown="1">

           ```shell
           postgres=# \l
           ```

           </div>

      2. ایجادیدن کاربر جدید:

         <div dir="ltr" align="left" markdown="1">

         ```shell
         postgres=# CREATE USER user_name WITH ENCRYPTED PASSWORD 'password';
         ```

         </div>

         - دیدن کاربرها:

           <div dir="ltr" align="left" markdown="1">

           ```shell
           postgres=# \du
           ```

           </div>

      3. اعتاییدن دسترسی کامل کاربر‌اِ به پایگاه‌داده‌اِ:

         <div dir="ltr" align="left" markdown="1">

         ```shell
         postgres=# GRANT ALL PRIVILEGES ON DATABASE db_name TO user_name;
         ```

         </div>

         - خروجیدن از شل:

           <div dir="ltr" align="left" markdown="1">

           ```shell
           postgres=# \q
           ```

           </div>

   3. نصیدن وابستگی‌ها:

      ```shell
      python -m pip install psycopg2
      ```

   4. پیکریدن برکا (app)

5. آمادن WSGI:

   1. نصبیدن gunicorn:

      <div dir="ltr" align="left" markdown="1">

      ```shell
      python -m pip install gunicorn
      # OR
      sudo apt install gunicorn
      ```

      </div>

      - استفیدن: کافیه فایل `wsgi` درگاه و فایل `wsgi` برکا بهش داده شود.

          <div dir="ltr" align="left" markdown="1">

        ```shell
        gunicorn -b 127.0.0.1:8000 app_name.wsgi
        ```

          </div>

   2. نهادن gunicorn در systemd:

      1. ایجادیدن فایل سرویس در یه مسیر دلخواه (`/etc/systemd/system/my_gunicorn.service`):

          <div dir="ltr" align="left" markdown="1">

         ```shell
         sudo nano /etc/systemd/system/my_gunicorn.service
         ```

         ```s
         [Unit]
         Description=Gunicorn service for app_name
         After=network.target

         [Service]
         Type=simple
         # Another Type: forking
         User=appuser  # Omit user/group if creating user service file
         Group=appuser
         WorkingDirectory=/app
         ExecStart=gunicorn -b 127.0.0.1:8000 app_name.wsgi
         Restart=on-failure
         # Other restart options: always, on-abort, etc

         # The install section is needed to use
         # `systemctl enable` to start on boot
         # For a user service that you want to enable
         # and start automatically, use `default.target`
         # For system level services, use `multi-user.target`
         [Install]
         WantedBy=multi-user.target
         ```

          </div>

   3. اجراییدن سرویس‌اِ:

      <div dir="ltr" align="left" markdown="1">

      ```shell
      systemctl start my_gunicorn.service
      systemctl enable my_gunicorn.service
      # بررسی
      systemctl status my_gunicorn.service
      netstat -ntlp
      ```

      </div>

6. آمادن سرور وب:

   1. نصبیدن nginx:

      <div dir="ltr" align="left" markdown="1">

      ```shell
      sudo apt install nginx
      # بررسی
      systemctl status nginx
      netstat -ntlp
      ```

      </div>

   2. پیکریدن nginx برای هدایتیدن درخواست‌ها:

      1. ایجادیدن `/etc/nginx/sites-available/app_name.conf` برای پیکریدن هاست مجازی برکا:

         <div dir="ltr" align="left" markdown="1">

         ```shell
         nano /etc/nginx/sites-available/app_name.conf
         ```

         </div>

      2. پیکریدن هاست مجازی:

         <div dir="ltr" align="left" markdown="1">

         ```conf
         # /etc/nginx/sites-available/app_name.conf

         # Redirect non-www to www.
         # server {
         #   listen 0.0.0.0:80;
         #   listen [::]:80;
         #   server_name app_name.com;

         #   return 301 http://www.app_name.com$request_uri;
         #}

         # Virtual host config for app_name
         server {
            listen 0.0.0.0:80;
            listen [::]:80;
            server_name app_name.com;

            location /robots.txt {
               alias /app/static/robots.txt;
            }

            location /favicon.ico {
               alias /app/static/favicon.ico;
            }

            # Static file
            location /static/ {
               alias /app/static/;
            }

            location /media/ {
               alias /app/uploads/;
            }

            # WSGI server
            location / {
               proxy_path http://127.0.0.1:800;
               proxy_set_header X-Real-IP $remote_addr;
            }

            access_log /var/log/nginx/app_name_access.log;
            error_log /var/log/nginx/app_name_error.log;
         }
         ```

         </div>

      3. نمادین‌پیوندین (Symbolic link) `/etc/nginx/sites-available/app_name.conf` در `/etc/nginx/sites-enabled/app_name.conf`

         <div dir="ltr" align="left" markdown="1">

         ```shell
         ls -s /etc/nginx/sites-{available,enabled}/app_name.conf
         ```

         </div>

      4. بازنشاندن nginx:

         <div dir="ltr" align="left" markdown="1">

         ```shell
         systemctl restart nginx
         # OR
         systemctl reload nginx
         ```

         </div>

      5. وارسیدن خطاهای احتمالی:

         <div dir="ltr" align="left" markdown="1">

         ```shell
         cat /vat/log/nginx/error.log
         ```

         </div>
